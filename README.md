# PhD Pipeline course materials (french)

Un atelier de PRAXIS autour de l'organisation du travail pendant la thèse tirant son inspiration de ma pratique personnelle.

1. [Atelier de 2019](2019/PhD_Pipline.pdf) :
   - Gérer son activité de veille
   - Versionner son travail 
   - Rythmer son travail en sprint
   - Accumuler des données
2. [Atelier de 2020](2020/index.html) : 
   - Structurer les connaissances accumulées (Zettlekasten)
   - Présenter son travail avec RevealJS, un framework opensource
3. [Atelier de 2021](2021/index.html)
   - Retour sur les ateliers précedents
   - Workflow pour rédiger sa thèse en Latex
     - Utiliser Overleaf pour la review
     - Utiliser VScode pour rédiger (avec les plugins qui vont bien pour traiter tout les aspects de la rédaction)
     - Sauvegarder sont manuscripts avec Syncthings